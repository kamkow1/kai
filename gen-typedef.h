#ifndef KAIC_GEN_TYPEDEF_H_
#define KAIC_GEN_TYPEDEF_H_

#include <string>
#include "c.h"

namespace kaic
{

struct Type;

struct Gen_Typedef : C
{
    std::string desired_name;
    Type * underlying;

    Gen_Typedef(std::string _desired_name, Type * _underlying):
        desired_name(_desired_name),
        underlying(_underlying)
    {
    }
    std::string c(void) override;
};

}; // namespace kaic

#endif // KAIC_GEN_TYPEDEF_H_
