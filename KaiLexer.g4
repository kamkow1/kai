lexer grammar KaiLexer;

channels {
    COMMENTS_CHANNEL
}

SINGLE_LINE_DOC_COMMENT     : '///' InputCharacter*   -> channel(COMMENTS_CHANNEL);
EMPTY_DELIMITED_DOC_COMMENT : '/***/'                 -> channel(COMMENTS_CHANNEL);
DELIMITED_DOC_COMMENT       : '/**' ~'/' .*? '*/'     -> channel(COMMENTS_CHANNEL);
SINGLE_LINE_COMMENT         : '//' InputCharacter*    -> channel(COMMENTS_CHANNEL);
DELIMITED_COMMENT           : '/*' .*? '*/'           -> channel(COMMENTS_CHANNEL);
WHITESPACES                 : (Whitespace | NewLine)+ -> channel(HIDDEN);

fragment NewLine: '\r\n' | '\r' | '\n';
fragment Whitespace: ' ' | '\t';
fragment InputCharacter: ~[\r\n\u0085\u2028\u2029];

STRUCT: 'struct';

STRING: '"' (~'"' | '\'\'')* '"';
INTEGER: '-'? '0'..'9'+;
FLOAT: '-'? ('0'..'9')+ '.' ('0'..'9')*;
IDENTIFIER: [a-zA-Z0-9_]+;
COLON: ':';
SEMICOLON: ';';
OPEN_PAREN: '(';
CLOSED_PAREN: ')';
OPEN_BRACE: '{';
CLOSED_BRACE: '}';
ARROW: '->';
ASTERISK: '*';
EQUALS: '=';
COMMA: ',';
AMPERSAND: '&';
HASH: '#';
DOT: '.';
DOLLAR: '$';

