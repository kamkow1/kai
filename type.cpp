#include <map>
#include <string>
#include "type.h"
#include "_assert.h"

using namespace kaic;

std::map<std::string, Type*> kaic::predefined_types = {
    {"Void",    new Basic_Type(NULL, "Void")   },
    {"Int8",    new Basic_Type(NULL, "Int8")   },
    {"Int16",   new Basic_Type(NULL, "Int16")  },
    {"Int32",   new Basic_Type(NULL, "Int32")  },
    {"Int64",   new Basic_Type(NULL, "Int64")  },
    {"Float32", new Basic_Type(NULL, "Float32")},
    {"Float64", new Basic_Type(NULL, "Float64")},
    {"String",  new Basic_Type(NULL, "String") },
};

bool kaic::check_type(Type * type1, Type * type2)
{
    if (type1->type_type == TT_BASIC && type2->type_type == TT_BASIC) {
        return ((Basic_Type *) (type1))->name == ((Basic_Type *) (type2))->name;
    }
    if (type1->type_type == TT_PTR && type2->type_type == TT_PTR) {
        return check_type(((Ptr_Type *) (type1))->ptr_type, ((Ptr_Type *) (type2))->ptr_type);
    }
    if (type1->type_type == TT_FUNC && type2->type_type == TT_FUNC) {
        auto func_type1 = (Func_Type *) (type1);
        auto func_type2 = (Func_Type *) (type2);
        auto ok = check_type(func_type1->rt_type, func_type2->rt_type);
        ok = ok && func_type1->params.size() == func_type2->params.size();
        for (size_t i = 0; i < func_type1->params.size(); i++) {
            ok = ok && check_type(func_type1->params[i]->param_type, func_type2->params[i]->param_type);
        }
        return ok;
    }
    return false;
}

Type * kaic::clone_type(Type *t)
{
    switch (t->type_type) {
        case TT_BASIC: {
                auto bt = (Basic_Type *) (t);
                auto nbt = new Basic_Type(bt->ir_module, bt->name);
                nbt->is_generic = bt->is_generic;
                return (Type *) (nbt);
            } break;
        case TT_PTR: {
                auto pt = (Ptr_Type *) (t);
                return (Type *) (new Ptr_Type(pt->ir_module, clone_type(pt->ptr_type)));
            } break;
        case TT_FUNC: {
                auto ft = (Func_Type *) (t);
                std::vector<Func_Type_Param *> params;
                for (auto ftp : ft->params) {
                    params.push_back(new Func_Type_Param(ftp->name, clone_type(ftp->param_type)));
                }
                auto nft = new Func_Type(ft->ir_module, clone_type(ft->rt_type), ft->generics);
                nft->params = params;
                return (Type *) (nft);
            } break;
        case TT_STRUCT: {
                auto st = (Struct_Type *) (t);
                std::vector<Struct_Field *> fields;
                for (auto sf : st->fields) {
                    fields.push_back(new Struct_Field(sf->name, clone_type(sf->field_type)));
                }
                auto nst = new Struct_Type(st->ir_module, fields, st->generics, new std::string(*st->name_ptr));
                nst->id = st->id;
                // TODO: Should this be cloned too?
                nst->generic_args = st->generic_args;
                return (Type *) (nst);
            } break;
        default: ASSERT(0, t->ir_module, "Unhandled type in %s\n", __PRETTY_FUNCTION__);
    }
}
