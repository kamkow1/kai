#ifndef KAIC_INSTR_H_
#define KAIC_INSTR_H_

#include <string>
#include "c.h"

namespace kaic
{

enum Instr_Type
{
    IT_VAR_DECL,
    IT_FUNC_DECL,
    IT_STRUCT_DECL,
    IT_VAR_ASSIGNMENT,
    IT_VALUE,
};

struct Instr : C
{
    Instr_Type instr_type;

    Instr(Instr_Type _instr_type):
        instr_type(_instr_type)
    {
    }
};

struct Value;

struct Var_Assingment : Instr
{
    std::string name;
    Value *value;

    Var_Assingment(std::string _name, Value * _value):
        Instr(IT_VAR_ASSIGNMENT),
        name(_name),
        value(_value)
    {
    }
    std::string c(void) override;
};

}; // namespace kaic

#endif // KAIC_INSTR_H_
