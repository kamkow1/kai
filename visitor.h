#ifndef KAIC_VISITOR_H_
#define KAIC_VISITOR_H_

#include <any>
#include "KaiParserBaseVisitor.h"
#include "ir-module.h"

namespace kaic
{

struct Visitor : KaiParserBaseVisitor
{
    IR_Module* ir_module;

    Visitor(IR_Module* _irmodule):
        ir_module(_irmodule)
    {
    }
    std::any visitFile(KaiParser::FileContext *ctx) override;
    std::any visitStatement(KaiParser::StatementContext *ctx) override;
    std::any visitFunctionDeclaration(KaiParser::FunctionDeclarationContext *ctx) override;
    std::any visitVariableDeclaration(KaiParser::VariableDeclarationContext *ctx) override;
    std::any visitStructDeclaration(KaiParser::StructDeclarationContext *ctx) override;
    std::any visitVariableAssignment(KaiParser::VariableAssignmentContext *ctx) override;
    std::any visitFunctionTypeParam(KaiParser::FunctionTypeParamContext *ctx) override;
    std::any visitType(KaiParser::TypeContext *ctx) override;
    std::any visitNameType(KaiParser::NameTypeContext *ctx) override;
    std::any visitFunctionType(KaiParser::FunctionTypeContext *ctx) override;
    std::any visitStructType(KaiParser::StructTypeContext *ctx) override;
    std::any visitIntegerExpression(KaiParser::IntegerExpressionContext *ctx) override;
    std::any visitFloatExpression(KaiParser::FloatExpressionContext *ctx) override;
    std::any visitCodeBlock(KaiParser::CodeBlockContext *ctx) override;
    std::any visitIdentifierExpression(KaiParser::IdentifierExpressionContext *ctx) override;
    std::any visitCallExpression(KaiParser::CallExpressionContext *ctx) override;
    std::any visitCallParamList(KaiParser::CallParamListContext *ctx) override;
    std::any visitEmphasizedExpression(KaiParser::EmphasizedExpressionContext *ctx) override;
    std::any visitAddressOfExpression(KaiParser::AddressOfExpressionContext *ctx) override;
    std::any visitDerefExpression(KaiParser::DerefExpressionContext *ctx) override;
    std::any visitStringExpression(KaiParser::StringExpressionContext *ctx) override;
    std::any visitStructInitExpression(KaiParser::StructInitExpressionContext *ctx) override;
    std::any visitDirective(KaiParser::DirectiveContext *ctx) override;
};

}; // namespace kaic

#endif // KAIC_VISITOR_H_
