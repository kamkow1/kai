parser grammar KaiParser;

options {
    tokenVocab = 'build/KaiLexer';
}

file: statement *;

statement: (declaration | variableAssignment | expression | directive) SEMICOLON;

// -----------------------------------------------------------------
// Declarations & Assignment
// -----------------------------------------------------------------
declaration: IDENTIFIER COLON (
        functionDeclaration
    |   variableDeclaration
    |   structDeclaration
);
functionDeclaration: functionType codeBlock?;
variableDeclaration: type? EQUALS expression;
structDeclaration: structType;

variableAssignment: IDENTIFIER EQUALS expression;

// -----------------------------------------------------------------
// Types
// -----------------------------------------------------------------
type: typeModifier* typeBase (OPEN_PAREN genericArgs CLOSED_PAREN)?;
typeModifier: ASTERISK;
typeBase: functionType | nameType;
functionType: (OPEN_PAREN generics CLOSED_PAREN)? OPEN_PAREN functionTypeParam * CLOSED_PAREN ARROW type;
functionTypeParam: IDENTIFIER COLON type;
nameType: (IDENTIFIER DOT)* IDENTIFIER;
structType: STRUCT (OPEN_PAREN generics CLOSED_PAREN)? OPEN_BRACE structFields? CLOSED_BRACE;
structFields: structField (COMMA structField)*;
structField: IDENTIFIER COLON type;
generics: IDENTIFIER (COMMA IDENTIFIER)*;
genericArgs: type (COMMA type)*;


codeBlock: OPEN_BRACE statement * CLOSED_BRACE;

// -----------------------------------------------------------------
// Expressions
// -----------------------------------------------------------------

expression:
        IDENTIFIER                                          #identifierExpression
    |   INTEGER (COLON type)?                               #integerExpression
    |   FLOAT (COLON type)?                                 #floatExpression
    |   STRING                                              #stringExpression
    |   OPEN_PAREN expression CLOSED_PAREN                  #emphasizedExpression
    |   AMPERSAND expression                                #addressOfExpression
    |   ASTERISK expression                                 #derefExpression
    |   type OPEN_BRACE structInitList? CLOSED_BRACE        #structInitExpression
    |   IDENTIFIER (OPEN_PAREN genericArgs CLOSED_PAREN)? OPEN_PAREN callParamList? CLOSED_PAREN   #callExpression
;

callParamList: expression (COMMA expression)*;
structInitList: structFieldInit (COMMA structFieldInit)*;
structFieldInit: DOT IDENTIFIER EQUALS expression;


// -----------------------------------------------------------------
// Directives
// -----------------------------------------------------------------

directive: HASH IDENTIFIER expression;

