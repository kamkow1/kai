// PREREQUISITE SOFTWARE:
//
// - Maven - tested on:
//      `mvn --version`
//      Apache Maven 3.6.3
//      Maven home: /usr/share/maven
//      Java version: 11.0.23, vendor: Debian, runtime: /usr/lib/jvm/java-11-openjdk-amd64
//      Default locale: en_US, platform encoding: UTF-8
//      OS name: "linux", version: "5.10.0-28-amd64", arch: "amd64", family: "unix"
//
// - Java - tested on:
//      `java --version`
//      openjdk 11.0.23 2024-04-16
//      OpenJDK Runtime Environment (build 11.0.23+9-post-Debian-1deb11u1)
//      OpenJDK 64-Bit Server VM (build 11.0.23+9-post-Debian-1deb11u1, mixed mode, sharing)
//
// - CMake - tested on:
//      `cmake --version`
//      cmake version 3.26.20230402-g17b8d01
//
//      CMake suite maintained and supported by Kitware (kitware.com/cmake).
//
// - Make - tested on:
//      `make --version`
//      GNU Make 4.3
//      Built for x86_64-pc-linux-gnu
//      Copyright (C) 1988-2020 Free Software Foundation, Inc.
//      License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
//      This is free software: you are free to change and redistribute it.
//      There is NO WARRANTY, to the extent permitted by law.

#define MIBS_IMPL
#define MIBS_USE_WRAPPERS
#include "mibs.h"

#ifdef MIBS_PLATFORM_UNIX
#   include <sys/sysinfo.h>
#   define NPROCS get_nprocs()
#else
#   define NPROCS 1 // TODO: implement NPROCS for windows as well
#endif

bool build_antlr4_tool(void)
{
    Mibs_Default_Allocator tmp_alloc = mibs_make_default_allocator();

    Mibs_Cmd cmd = {0};
    mdefer { mibs_da_deinit(&tmp_alloc, &cmd); }
    mibs_cmd_append(&tmp_alloc, &cmd, "mvn", "-DskipTests", "clean", "package");
    bool ok = mibs_run_cmd(&tmp_alloc, &cmd, MIBS_CMD_SYNC, "antlr4/tool/").ok;
    ok = ok && mibs_copy_file(
        "antlr4/tool/target/antlr4-4.13.2-SNAPSHOT-complete.jar",
        "build/antlr4.jar",
        MIBS_FK_BIN
    );
    return ok;
}

bool build_antlr4_cpp_runtime(void)
{
    Mibs_Default_Allocator tmp_alloc = mibs_make_default_allocator();

    if (!mPATH_EXISTS("antlr4/runtime/Cpp/build")) {
        mMKDIR("antlr4/runtime/Cpp/build");
    }

    Mibs_Cmd cmake_cmd = {0};
    mdefer { mibs_da_deinit(&tmp_alloc, &cmake_cmd); }
    mibs_cmd_append(&tmp_alloc, &cmake_cmd, "cmake", "-DCMAKE_BUILD_TYPE=Debug",
        "-Bantlr4/runtime/Cpp/build", "-Santlr4/runtime/Cpp/"
    );
    bool ok = mibs_run_cmd(&tmp_alloc, &cmake_cmd, MIBS_CMD_SYNC, 0).ok;

    Mibs_Cmd make_cmd = {0};
    mdefer { mibs_da_deinit(&tmp_alloc, &make_cmd); }
    char nprocs[3];
    memset(nprocs, 0, 3);
    sprintf(nprocs, "%d", NPROCS);
    mibs_cmd_append(&tmp_alloc, &make_cmd, "make", "-j", nprocs);
    ok = ok && mibs_run_cmd(&tmp_alloc, &make_cmd, MIBS_CMD_SYNC, "antlr4/runtime/Cpp/build").ok;

    ok = ok && mibs_copy_file(
        "antlr4/runtime/Cpp/dist/libantlr4-runtime.a",
        "build/libantlr4-runtime.a",
        MIBS_FK_BIN
    );
    return ok;
}

bool generate_kai_lexer(void)
{
    #define KAIC_LEXER_FLAGS "-Dlanguage=Cpp", "-package", "kaic", "-o", "build/", \
        "-no-visitor", "-no-listener"

    return mCMD("java", "-cp", "build/antlr4.jar", "org.antlr.v4.Tool", KAIC_LEXER_FLAGS, "KaiLexer.g4");
}

bool generate_kai_parser(void)
{
    #define KAIC_PARSER_FLAGS "-Dlanguage=Cpp", "-package", "kaic", "-o", "build/", \
        "-visitor", "-no-listener"
    return mCMD("java", "-cp", "build/antlr4.jar", "org.antlr.v4.Tool", KAIC_PARSER_FLAGS, "KaiParser.g4");
}

bool generate_kai_parser_and_lexer(void)
{
    return generate_kai_lexer() && generate_kai_parser();
}

bool build_kaic(void)
{
    if (!mPATH_EXISTS("build/")) {
        mMKDIR("build/");
    }

    if (!mPATH_EXISTS("build/antlr4.jar")) {
        if (!build_antlr4_tool()) return 1;
    }

    if (!mPATH_EXISTS("build/libantlr4-runtime.a")) {
        if (!build_antlr4_cpp_runtime()) return 1;
    }

    if (!mPATH_EXISTS("build/KaiLexer.h") || !mPATH_EXISTS("build/KaiLexer.cpp")) {
        if (!generate_kai_lexer()) return 1;
    }

    if (!mPATH_EXISTS("build/KaiParser.h")
        || !mPATH_EXISTS("build/KaiParser.cpp")
        || !mPATH_EXISTS("build/KaiParserVisitor.h")
        || !mPATH_EXISTS("build/KaiParserVisitor.cpp")
        || !mPATH_EXISTS("build/KaiParserBaseVisitor.h")
        || !mPATH_EXISTS("build/KaiParserBaseVisitor.cpp")) {
        if (!generate_kai_parser()) return 1;
    }

    Mibs_Default_Allocator tmp_alloc = mibs_make_default_allocator();

    // source path
    char *cwd = getcwd(0, 0);
    Mibs_String_Builder sb = {0};
    mdefer {
        free(cwd);
        mibs_da_deinit(&tmp_alloc, &sb);
    }
    mibs_sb_append_cstr(&tmp_alloc, &sb, "-D_KAIC_SOURCE_PATH=\"");
    mibs_sb_append_cstr(&tmp_alloc, &sb, cwd);
    mibs_sb_append_cstr(&tmp_alloc, &sb, "\"");
    const char * kaic_source_path = sb.items;

    #define KAIC_SRC                    \
        "main.cpp",                     \
        "visitor.cpp",                  \
        "type.cpp",                     \
        "instr-c.cpp",                  \
        "ir-module.cpp",                \
        "decl-c.cpp",                   \
        "directive.cpp",                \
        "gen-typedef.cpp",              \
        "backward.cpp",                 \
        "build/KaiLexer.cpp",           \
        "build/KaiParser.cpp",          \
        "build/KaiParserVisitor.cpp",   \
        "build/KaiParserBaseVisitor.cpp"
    #define KAIC_CFLAGS "-Wall", "-Wextra", "-std=c++17", "-pthread", "-ggdb"
    #define KAIC_CPP_FLAGS          \
        kaic_source_path,           \
        "-DBACKWARD_HAS_BFD=1"
    #define KAIC_INCLUDES "-I.", "-Iantlr4/runtime/Cpp/runtime/src/", "-Ibuild/"
    #define KAIC_LD_PATHS "-Lbuild/"
    #define KAIC_LDFLAGS "-l:libantlr4-runtime.a", "-ldl", "-lbfd", "-rdynamic"

    return mCMD("g++", KAIC_CFLAGS, KAIC_INCLUDES, KAIC_CPP_FLAGS, "-o", "build/kaic", KAIC_SRC, KAIC_LD_PATHS, KAIC_LDFLAGS);
}

bool clean(void)
{
    return mRMDIR("build/");
}

int main(int argc,char** argv)
{
    mREBUILD(argc, argv);

    if (argc < 2) {
        mibs_log(MIBS_LL_ERROR, "Please provide a command\n");
        return 1;
    }

    const char* cmd = *(++argv);
    
    if (strcmp(cmd, "build") == 0) {
        if (!build_kaic()) return 1;
    } else if (strcmp(cmd, "clean") == 0) {
        if (!clean()) return 1;
    } else if (strcmp(cmd, "generate") == 0) {
        if (!generate_kai_parser_and_lexer()) return 1;
    }


    return 0;
}

