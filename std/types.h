#ifndef KAI_STD_TYPES_H_
#define KAI_STD_TYPES_H_

#define Int8()  char
#define Int16() short
#define Int32() int
#define Int64() long long

#define Float32()   float
#define Float64()   double

#define Void()  void

#endif // KAI_STD_TYPES_H_
