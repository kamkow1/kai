#ifndef KAIC_TYPE_H_
#define KAIC_TYPE_H_

#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include "c.h"
#include "log.h"
#include "ir-module.h"
#include "_assert.h"
#include "gen-typedef.h"

namespace kaic
{

enum Type_Type
{
    TT_BASIC,
    TT_PTR,
    TT_FUNC,
    TT_STRUCT,
};

struct Type : C
{
    IR_Module *ir_module;
    Type_Type type_type;

    Type(IR_Module *_ir_module, Type_Type _type_type):
        ir_module(_ir_module),
        type_type(_type_type)
    {
    }
};

bool check_type(Type * type1, Type * type2);
Type * clone_type(Type *t);

struct Struct_Field : C
{
    std::string name;
    Type *field_type;

    Struct_Field(std::string _name, Type *_field_type):
        name(_name),
        field_type(_field_type)
    {
    }
    std::string c(void) override
    {
        std::string s;
        s += field_type->c();
        s += " ";
        s += this->name;
        return s;
    }
};

struct Struct_Type : Type 
{
    std::vector<Struct_Field *> fields;
    std::vector<std::string> generics;
    std::vector<Type *> generic_args;
    std::string *name_ptr = NULL;
    bool is_ref = false; // is a reference to a struct type or a type within a declaration
    std::string id;

    Struct_Type(IR_Module *_ir_module,
            std::vector<Struct_Field *> _fields,
            std::vector<std::string> _generics,
            std::string *_name_ptr):
        Type(_ir_module, TT_STRUCT),
        fields(_fields),
        generics(_generics),
        name_ptr(_name_ptr),
        id(this->random_struct_id())
    {
        auto gt = new Gen_Typedef(this->id, (Type *) (this));
        this->Type::ir_module->gen_typedefs.push_back(gt);
    }
    std::string random_struct_id(void)
    {
        std::string s;
        #define RANDOM_STRUCT_ID_SIZE 0xf
        const char chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        for (size_t i = 0; i < RANDOM_STRUCT_ID_SIZE; i++) {
            int k = rand()%(int)(sizeof(chars)-1);
            s += chars[k];
        }
        return s;
    }
    std::string c(void) override
    {
        std::string s;

        if (this->is_ref) {
            s += this->c_name();
            s += "(";
            size_t i = 0;
            for (auto ga : this->generic_args) {
                s += ga->c();
                if (i != this->generic_args.size()-1) {
                    s += ", ";
                }
                i++;
            }
            s += ")";
        } else {
            return this->id;
        }

        return s;
    }
    std::string c_name(void) override
    {
        std::string s = *this->name_ptr;
        std::replace(s.begin(),s.end(),'.','_');
        return s;
    }
};

struct Ptr_Type : Type
{
    Type *ptr_type;
    
    Ptr_Type(IR_Module *_ir_module, Type *_ptr_type):
        Type(_ir_module, TT_PTR),
        ptr_type(clone_type(_ptr_type))
    {
        if (this->ptr_type->type_type == TT_STRUCT) {
            ((Struct_Type *) (this->ptr_type))->is_ref = true;
        }
    }
    std::string c(void) override
    {
        std::string s;
        s += this->ptr_type->c();
        s += "*";
        return s;
    }
};

struct Basic_Type : Type
{
    std::string name;
    bool is_generic = false; // is a generic type, eg. 'T'

    Basic_Type(IR_Module *_ir_module, std::string _name):
        Type(_ir_module, TT_BASIC),
        name(_name)
    {
        #if 0
        // is predefined type
        if (this->Type::ir_module == NULL) return;
        printf("%zu\n", this->Type::ir_module->generics_stack.size());
        ASSERT(this->Type::ir_module->get_type(this->name)
            || this->Type::ir_module->get_type_from_current_scope(this->name)
            || this->Type::ir_module->param_in_generics_stack(this->name),
            this->Type::ir_module->current_line,
            "Unknown type %s\n", this->name.c_str());
        #endif
    }
    std::string c(void) override
    {
        std::string s;
        s += this->c_name();
        if (!this->is_generic) s += "()";
        return s;
    }
    std::string c_name(void) override
    {
        std::string s = this->name;
        std::replace(s.begin(),s.end(),'.','_');
        return s;
    }
};

struct Func_Type_Param : C
{
    std::string name;
    Type *param_type;

    Func_Type_Param(std::string _name, Type* _param_type):
        name(_name),
        param_type(_param_type)
    {
    }
    std::string c(void) override
    {
        std::string s;
        s += this->param_type->c();
        s += " ";
        s += this->name;
        return s;
    }
};

struct Func_Type : Type
{
    Type *rt_type;
    std::vector<Func_Type_Param *> params;
    std::vector<std::string> generics;

    Func_Type(IR_Module *_ir_module,
            Type* _rt_type,
            std::vector<std::string> _generics):
        Type(_ir_module, TT_FUNC),
        rt_type(_rt_type),
        generics(_generics)
    {
    }
    std::string c(void) override
    {
        ASSERT(0, this->Type::ir_module, "Func_Type cannot be trivially converted to C\n");
        return "";
    }
};

extern std::map<std::string, Type*> predefined_types;

}; // namespace kaic

#endif // KAIC_TYPE_H_
