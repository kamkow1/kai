#include <any>
#include <algorithm>
#include <stdio.h>
#include "KaiParser.h"
#include "visitor.h"
#include "type.h"
#include "value.h"
#include "instr.h"
#include "decl.h"
#include "_assert.h"
#include "directive.h"

#define IR_MODULE() ({this->ir_module->current_line = ctx->start->getLine(); this->ir_module;})

using namespace kaic;

std::any Visitor::visitFile(KaiParser::FileContext *ctx)
{
    for (KaiParser::StatementContext *s: ctx->statement()) {
        this->visitStatement(s);
    }
    return 0;
}
    
std::any Visitor::visitCodeBlock(KaiParser::CodeBlockContext *ctx)
{
    this->ir_module->make_scope();
    std::vector<Instr*> instrs;
    for (KaiParser::StatementContext *s: ctx->statement()) {
        instrs.push_back(std::any_cast<Instr*>(this->visitStatement(s)));
    }
    this->ir_module->drop_scope();
    return instrs;
}

// TODO: figure out why do I have to redirect calls to child nodes
std::any Visitor::visitStatement(KaiParser::StatementContext *ctx)
{
    if(ctx->declaration())
        return this->visitDeclaration(ctx->declaration());
    if(ctx->variableAssignment())
        return this->visitVariableAssignment(ctx->variableAssignment());
    if (ctx->expression())
        return (Instr *) (std::any_cast<Value*>(this->visit(ctx->expression())));
    if (ctx->directive())
        return this->visitDirective(ctx->directive());

    ASSERT(0,IR_MODULE(), "TODO: Unimplemented statement %s\n", ctx->getText().c_str());
    return 0;
}

// -----------------------------------------------------------------
// Declarations & Assignment
// -----------------------------------------------------------------

std::any Visitor::visitFunctionDeclaration(KaiParser::FunctionDeclarationContext *ctx)
{
    KaiParser::DeclarationContext* parent = dynamic_cast<KaiParser::DeclarationContext*>(ctx->parent);
    auto name = parent->IDENTIFIER()->toString();
    auto type = std::any_cast<Func_Type*>(this->visitFunctionType(ctx->functionType()));
    auto fdecl = new Func_Decl(IR_MODULE(), name, type);

    if (ctx->codeBlock()) {
        fdecl->has_defined_body = true;
        fdecl->instrs = std::any_cast<std::vector<Instr*>>(this->visitCodeBlock(ctx->codeBlock()));
    }
    
    // declaration <- statement <- file / code block
    auto parent_2 = (antlr4::ParserRuleContext *)parent->parent;
    if (((antlr4::ParserRuleContext *) (parent_2->parent))->getRuleIndex() == KaiParser::RuleFile) {
        // inside of file context, treat as a global variable
        // if not, treat as a local variable
        Decl * decl = IR_MODULE()->get_decl(name);
        ASSERT(!decl, IR_MODULE(), "%s is already declared globally\n", name.c_str());
        this->ir_module->decls.push_back((Decl *) (fdecl));
    } else {
        Decl * decl = IR_MODULE()->get_decl_from_current_scope(name);
        ASSERT(!decl, IR_MODULE(), "%s is already declared locally\n", name.c_str());
        this->ir_module->get_current_scope()->decls.push_back((Decl *) (fdecl));
    }

    return (Instr *) (fdecl);
}
    
std::any Visitor::visitVariableDeclaration(KaiParser::VariableDeclarationContext *ctx)
{
    auto parent = (KaiParser::DeclarationContext *) (ctx->parent);
    auto name = parent->IDENTIFIER()->toString();
    auto value = std::any_cast<Value*>(this->visit(ctx->expression()));
    Type * type = NULL;
    if (ctx->type()) {
        type = std::any_cast<Type*>(this->visitType(ctx->type()));
    } else {
        type = value->type;
    }
    type = clone_type(type);
    if (type->type_type == TT_STRUCT) {
        ((Struct_Type *)(type))->is_ref = true;
    }

    auto vdecl = new Var_Decl(IR_MODULE(), name, type, value);

    // declaration <- statement <- file / code block
    auto parent_2 = (antlr4::ParserRuleContext *)parent->parent;
    if (((antlr4::ParserRuleContext *) (parent_2->parent))->getRuleIndex() == KaiParser::RuleFile) {
        // inside of file context, treat as a global variable
        // if not, treat as a local variable
        Decl * decl = IR_MODULE()->get_decl(name);
        ASSERT(!decl, IR_MODULE(), "%s is already declared globally\n", name.c_str());
        ASSERT(value->value_type != VT_CALL, IR_MODULE(),
            "Cannot assign a function call result to %s, because i'ts in a global scope\n", name.c_str());
        this->ir_module->decls.push_back((Decl *) (vdecl));
    } else {
        Decl * decl = IR_MODULE()->get_decl_from_current_scope(name);
        ASSERT(!decl, IR_MODULE(), "%s is already declared locally\n", name.c_str());
        this->ir_module->get_current_scope()->decls.push_back((Decl *) (vdecl));
    }

    return (Instr *)vdecl;
}
    
std::any Visitor::visitStructDeclaration(KaiParser::StructDeclarationContext *ctx)
{
    auto parent = (KaiParser::DeclarationContext *) (ctx->parent);
    auto name = parent->IDENTIFIER()->toString();
    name = IR_MODULE()->package_id + name;
    auto struct_type = (Struct_Type *)std::any_cast<Type *>(this->visitStructType(ctx->structType()));
    auto sdecl = new Struct_Decl(IR_MODULE(), name, struct_type);
    this->ir_module->decls.push_back((Decl *) (sdecl));
    this->ir_module->types.push_back((Type *) (struct_type));
    return (Instr *) sdecl;
}
    
std::any Visitor::visitVariableAssignment(KaiParser::VariableAssignmentContext *ctx)
{
    auto name = ctx->IDENTIFIER()->toString();
    auto value = std::any_cast<Value *>(this->visit(ctx->expression()));
    return (Instr *) (new Var_Assingment(name, value));
}

// -----------------------------------------------------------------
// Types
// -----------------------------------------------------------------

std::any Visitor::visitType(KaiParser::TypeContext *ctx)
{
    auto type = std::any_cast<Type*>(this->visitTypeBase(ctx->typeBase()));
    if (ctx->genericArgs()) {
        if (type->type_type == TT_BASIC) {
            auto bt = (Basic_Type *) (type);
            type = IR_MODULE()->get_type(bt->name);
            if (!type) { type = IR_MODULE()->get_type_from_current_scope(bt->name); }
            ASSERT(type, IR_MODULE(), "Unknown type %s\n", bt->name.c_str());
        }

        ASSERT(type->type_type == TT_STRUCT, IR_MODULE(),
            "Only struct types can be initialized generically\n");
        auto st = (Struct_Type *) (type);
        std::vector<Type *> generic_args;
        for (auto t : ctx->genericArgs()->type()) {
            auto arg = std::any_cast<Type *>(this->visitType(t));
            arg = clone_type(arg);
            if (arg->type_type == TT_STRUCT) {
                ((Struct_Type *) (arg))->is_ref = true;
            }
            generic_args.push_back(arg);
        }
        st->generic_args = generic_args;
    }

    std::vector<KaiParser::TypeModifierContext*> tmcs = ctx->typeModifier();
    std::reverse(tmcs.begin(), tmcs.end());
    for (KaiParser::TypeModifierContext* tmc : tmcs) {
        if (tmc->ASTERISK()) {
            type = new Ptr_Type(IR_MODULE(), type);
        }
    }
    return type;
}

std::any Visitor::visitFunctionTypeParam(KaiParser::FunctionTypeParamContext *ctx)
{
    auto param_type = std::any_cast<Type*>(this->visitType(ctx->type()));
    param_type = clone_type(param_type);
    if (param_type->type_type == TT_STRUCT) {
        ((Struct_Type *)(param_type))->is_ref = true;
    }
    auto ftp = new Func_Type_Param(ctx->IDENTIFIER()->toString(), param_type);
    return ftp;
}
    
std::any Visitor::visitFunctionType(KaiParser::FunctionTypeContext *ctx)
{
    std::vector<std::string> generics;
    if (ctx->generics()) {
        for (auto g : ctx->generics()->IDENTIFIER()) {
            generics.push_back(g->toString());
        }
    }
    this->ir_module->generics_stack.push_back(generics);
    auto t = std::any_cast<Type*>(this->visitType(ctx->type()));
    auto ft = new Func_Type(IR_MODULE(),t,generics);
    std::vector<Func_Type_Param *> params;
    for (KaiParser::FunctionTypeParamContext* ftpctx: ctx->functionTypeParam()) {
        params.push_back(std::any_cast<Func_Type_Param *>(this->visitFunctionTypeParam(ftpctx)));
    }
    ft->params = params;
    this->ir_module->generics_stack.pop_back();
    return ft;
}

std::any Visitor::visitNameType(KaiParser::NameTypeContext *ctx)
{
    std::string name;
    size_t i = 0;
    for (auto id : ctx->IDENTIFIER()) {
        name += id->toString();
        if (i != ctx->IDENTIFIER().size()-1) {
            name += ".";
        }
        i++;
    }
    if (predefined_types.find(name) != predefined_types.end()) return predefined_types.at(name);
    auto nbt = new Basic_Type(IR_MODULE(), name);
    if (IR_MODULE()->param_in_generics_stack(name)) {
        nbt->is_generic = true;
    }
    return (Type*)(nbt);
}
  
std::any Visitor::visitStructType(KaiParser::StructTypeContext *ctx)
{
    std::vector<std::string> generics;
    if (ctx->generics()) {
        for (auto g : ctx->generics()->IDENTIFIER()) {
            generics.push_back(g->toString());
        }
    }
    this->ir_module->generics_stack.push_back(generics);
    std::vector<Struct_Field *> fields;
    if (ctx->structFields()) {
        for (auto sf : ctx->structFields()->structField()) {
            auto field_name = sf->IDENTIFIER()->toString();
            auto field_type = std::any_cast<Type *>(this->visitType(sf->type()));
            fields.push_back(new Struct_Field(field_name,field_type));
        }
    }
    this->ir_module->generics_stack.pop_back();
    std::string *name_ptr = NULL;
    auto parent = (antlr4::ParserRuleContext *) (ctx->parent);
    if (parent->getRuleIndex() == KaiParser::RuleStructDeclaration) {
        auto parent_2 = (antlr4::ParserRuleContext *)parent->parent;
        auto decl_name = ((KaiParser::DeclarationContext *) (parent_2))->IDENTIFIER()->toString();
        auto package = IR_MODULE()->package_id;
        auto name = (package.size() > 0 ? package : "") + decl_name;
        name_ptr = new std::string(name);
    }
    return (Type *) (new Struct_Type(IR_MODULE(), fields, generics, name_ptr));
}

// -----------------------------------------------------------------
// Expressions
// -----------------------------------------------------------------

std::any Visitor::visitIntegerExpression(KaiParser::IntegerExpressionContext *ctx)
{
    if (ctx->type()) {
        auto type = std::any_cast<Type *>(this->visitType(ctx->type()));
        ASSERT(type->type_type == TT_BASIC, IR_MODULE(),
            "Integer literal type annotation is not a basic type\n");
        std::string type_string = ((Basic_Type *) (type))->name;
        ASSERT(predefined_types.find(type_string) != predefined_types.end(), IR_MODULE(),
            "Integer literal type annotation is not one of builtin integer types: Int8, Int16, Int32, Int64\n");
        std::string width_string = type_string.substr(3, type_string.size()-2);
        int width = std::stoi(width_string);
        switch(width) {
        case 8*sizeof(char): return (Value *) (new Int_Value<char>(IR_MODULE(), std::stoi(ctx->INTEGER()->toString())));
        case 8*sizeof(short): return (Value *) (new Int_Value<short>(IR_MODULE(), std::stoi(ctx->INTEGER()->toString())));
        case 8*sizeof(int): return (Value *) (new Int_Value<int>(IR_MODULE(), std::stoi(ctx->INTEGER()->toString())));
        case 8*sizeof(long long): return (Value *) (new Int_Value<long long>(IR_MODULE(), std::stoi(ctx->INTEGER()->toString())));
        }
    }
    return (Value *) (new Int_Value<int>(IR_MODULE(), std::stoi(ctx->INTEGER()->toString())));
}
    
std::any Visitor::visitFloatExpression(KaiParser::FloatExpressionContext *ctx)
{
    if (ctx->type()) {
        auto type = std::any_cast<Type *>(this->visitType(ctx->type()));
        ASSERT(type->type_type == TT_BASIC, IR_MODULE(),
            "Float literal type annotation is not a basic type\n");
        std::string type_string = ((Basic_Type *) (type))->name;
        ASSERT(predefined_types.find(type_string) != predefined_types.end(), IR_MODULE(),
            "Float literal type annotation is not one of builtin integer types: Float32, Float64\n");
        std::string width_string = type_string.substr(5, type_string.size()-2);
        int width = std::stoi(width_string);
        switch(width) {
        case 8*sizeof(float): return (Value *) (new Float_Value<float>(IR_MODULE(), std::stoi(ctx->FLOAT()->toString())));
        case 8*sizeof(double): return (Value *) (new Float_Value<double>(IR_MODULE(), std::stoi(ctx->FLOAT()->toString())));
        }
    }
    return (Value *) (new Float_Value<double>(IR_MODULE(), std::stod(ctx->FLOAT()->toString())));
}

std::any Visitor::visitIdentifierExpression(KaiParser::IdentifierExpressionContext *ctx)
{
    return (Value *) (new Ref_Value(IR_MODULE(), ctx->IDENTIFIER()->toString()));
}

std::any Visitor::visitCallExpression(KaiParser::CallExpressionContext *ctx)
{
    auto name = ctx->IDENTIFIER()->toString();

    std::vector<Type *> generic_args;
    if (ctx->genericArgs()) {
        for (auto t : ctx->genericArgs()->type()) {
            auto arg = std::any_cast<Type *>(this->visitType(t));
            arg = clone_type(arg);
            if (arg->type_type == TT_STRUCT) {
                ((Struct_Type *) (arg))->is_ref = true;
            }
            generic_args.push_back(arg);
        }
    }

    std::vector<Value *> args;
    if (ctx->callParamList()) {
        args = std::any_cast<std::vector<Value *>>(this->visitCallParamList(ctx->callParamList()));
    }
    return (Value *) (new Call_Value(IR_MODULE(), name, args, generic_args));
}
    
std::any Visitor::visitCallParamList(KaiParser::CallParamListContext *ctx)
{
    std::vector<Value *> values;
    for (KaiParser::ExpressionContext *expr : ctx->expression()) {
        values.push_back(std::any_cast<Value *>(this->visit(expr)));
    }
    return values;
}

std::any Visitor::visitEmphasizedExpression(KaiParser::EmphasizedExpressionContext *ctx)
{
    return this->visit(ctx->expression());
}
    
std::any Visitor::visitAddressOfExpression(KaiParser::AddressOfExpressionContext *ctx)
{
    auto value = std::any_cast<Value *>(this->visit(ctx->expression()));
    return (Value *) (new Addr_Value(IR_MODULE(), value));
}

std::any Visitor::visitDerefExpression(KaiParser::DerefExpressionContext *ctx)
{
    auto value = std::any_cast<Value *>(this->visit(ctx->expression()));
    return (Value *) (new Deref_Value(IR_MODULE(), value));
}

std::any Visitor::visitStringExpression(KaiParser::StringExpressionContext *ctx)
{
    auto string = ctx->STRING()->toString();
    return (Value *) (new String_Value(IR_MODULE(), string));
}
  
std::any Visitor::visitStructInitExpression(KaiParser::StructInitExpressionContext *ctx)
{
    auto type = std::any_cast<Type *>(this->visitType(ctx->type()));
    std::vector<Struct_Field_Init> field_inits;
    if (ctx->structInitList()) {
        for (auto field_init : ctx->structInitList()->structFieldInit()) {
            auto field_name = field_init->IDENTIFIER()->toString();
            auto value = std::any_cast<Value *>(this->visit(field_init->expression()));
            field_inits.push_back(Struct_Field_Init(field_name, value));
        }
    }
    return (Value *) (new Struct_Value(IR_MODULE(), type, field_inits));
}
    
// -----------------------------------------------------------------
// Directives
// -----------------------------------------------------------------

std::any Visitor::visitDirective(KaiParser::DirectiveContext *ctx)
{
    auto name = ctx->IDENTIFIER()->toString();
    ASSERT(directives.find(name) != directives.end(), IR_MODULE(), "Unknown directive %s\n", name.c_str());
    if (name == "import") {
        auto value = std::any_cast<Value *>(this->visit(ctx->expression()));
        ASSERT(value->value_type == VT_STRING, IR_MODULE(), "Import path must be a string\n");
        auto string = (String_Value *) (value);
        ((Directive<IR_Module *, std::string> *) (directives.at(name)))->action(IR_MODULE(), string->_string);
    } else if (name == "import_c") {
        auto value = std::any_cast<Value *>(this->visit(ctx->expression()));
        ASSERT(value->value_type == VT_STRING, IR_MODULE(), "Import path must be a string\n");
        auto string = (String_Value *) (value);
        ((Directive<IR_Module *, std::string> *) (directives.at(name)))->action(IR_MODULE(), string->_string);
    } else if (name == "package") {
        auto value = std::any_cast<Value *>(this->visit(ctx->expression()));
        ASSERT(value->value_type == VT_STRING, IR_MODULE(), "Package ID must be a string\n");
        auto string = (String_Value *) (value);
        ((Directive<IR_Module *, std::string> *) (directives.at(name)))->action(IR_MODULE(), string->_string);
    } else {
        ASSERT(0,IR_MODULE(), "Unhandled directive %s\n", name.c_str());
    }
    return 0;
}

