#include <string>
#include "decl.h"
#include "value.h"
#include "type.h"
#include "gen-typedef.h"

using namespace kaic;

std::string Var_Decl::c(void)
{
    std::string s;

    if (this->var_type->type_type == TT_FUNC) {
        auto ftype = (Func_Type *) (this->var_type);
        s += ftype->rt_type->c();
        s += " (";
        s += this->Decl::name;
        s += ") ";
        s += "(";
        size_t i = 0;
        for (auto param : ftype->params) {
            s += param->param_type->c();
            if (i != ftype->params.size()-1) {
                s += ", ";
            }
            i++;
        }
        s += ")";
    } else if (this->var_type->type_type == TT_STRUCT) {
        auto st = (Struct_Type *) (this->var_type);
        if (st->generic_args.size() > 0) {
            s += st->id;
            auto gt = new Gen_Typedef(st->id, (Type *) (st));
            this->Decl::ir_module->gen_typedefs.push_back(gt);
        }
        s += " ";
        s += this->Decl::name;
    } else {
        s += this->var_type->c();
        s += " ";
        s += this->Decl::name;
    }
    s += " = ";
    s += this->value->c();
    s += ";";
    
    // deinitialize, if string
    if (this->value->value_type == VT_STRING) {
        s += "\n";
        s += "defer { dynamic_array_deinit(&allocator, &" + this->name + "); }";
    }
    return s;
}

