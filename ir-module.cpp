#include <string>
#include <fstream>
#include <sstream>
#include <filesystem>
#include "ir-module.h"
#include "decl.h"
#include "config.h"

using namespace kaic;
    
Decl * IR_Module::get_decl(std::string name)
{
    Decl * decl = NULL;
    for (auto _decl : this->decls) {
        if (_decl->name == name) {
            decl = _decl;
            break;
        }
    }
    return decl;
}

Type * IR_Module::get_type(std::string name)
{
    Type * type = NULL;
    for (auto _type : this->types) {
        if (_type->type_type == TT_BASIC) {
            if (((Basic_Type *) _type)->name == name) { type = _type; break; }
        } else if (_type->type_type == TT_STRUCT) {
            if (this->get_decl(name)) { type = _type; break; }
        }
    }
    return type;
}
    
void IR_Module::make_scope(void)
{
    this->scopes.push_back(new Scope);
}

void IR_Module::drop_scope(void)
{
    this->scopes.pop_back();
}

Scope * IR_Module::get_current_scope(void)
{
    if (this->scopes.size() == 0) return NULL;
    return this->scopes.back();
}
    
Decl *IR_Module::get_decl_from_current_scope(std::string name)
{
    Decl * decl = NULL;
    Scope *s = this->get_current_scope();
    for (auto _decl : s->decls) {
        if (_decl->name == name) { decl = _decl; break; }
    }
    return decl;
}

Type *IR_Module::get_type_from_current_scope(std::string name)
{
    Type * type = NULL;
    Scope *s = this->get_current_scope();
    if (!s) return NULL;
    for (auto _type : s->types) {
        if (_type->type_type == TT_BASIC) {
            if (((Basic_Type *) _type)->name == name) { type = _type; break; }
        } else if (_type->type_type == TT_STRUCT) {
            if (this->get_decl_from_current_scope(name)) { type = _type; break; }
        }
    }
    return type;
}
    
std::string *IR_Module::param_in_generics_stack(std::string name)
{
    std::string * p = NULL;
    for (auto level : this->generics_stack) {
        for (auto g : level) {
            if (g == name) { p = &g; break; }
        }
    }
    return p;
}

static std::string includes[] = {
    KAIC_STD_PATH + "/types.h",
    /* KAIC_STD_PATH + "/String.h", */
    /* KAIC_STD_PATH + "/defer.h", */
};

std::string IR_Module::c(void)
{
    std::string s;

    for (auto inc : includes) { s += "#include \"" + inc + "\"\n"; }
    for (auto inc : this->additional_includes) { s += "#include \"" + inc + "\"\n"; }
    for (auto decl : this->decls) { if (decl->decl_type == DT_STRUCT) s += decl->c() + "\n"; }
    for (auto gt : this->gen_typedefs) { s += gt->c() + "\n"; }
    for (auto decl : this->decls) { if (decl->decl_type != DT_STRUCT) s += decl->c() + "\n"; }

    if (this->is_root) {
        std::filesystem::path std = KAIC_STD_PATH;
        std::filesystem::path mainh_path = std / "main.h";
        std::ifstream mainh_file(mainh_path.string());
        std::string mainh;
        std::stringstream buffer;
        buffer << mainh_file.rdbuf();
        mainh = buffer.str();

        s += mainh;
    }

    return s;
}
    
Gen_Typedef * IR_Module::get_gen_typedef(std::string id)
{
    for (auto gt : this->gen_typedefs) {
        if (((Struct_Type *)(gt->underlying))->id == id) { return gt; }
    }
    return NULL;
}

