#include <string>
#include <map>
#include <filesystem>
#include "antlr4-runtime.h"
#include "KaiLexer.h"
#include "KaiParser.h"
#include "directive.h"
#include "config.h"
#include "visitor.h"
#include "ir-module.h"
#include "decl.h"

using namespace kaic;

std::map<std::string, kaic::Directive_Base *> kaic::directives = {
    {"import", (Directive_Base *) new Directive<IR_Module *, std::string>("import", DT_IMPORT, &import_action)},
    {"import_c", (Directive_Base *) new Directive<IR_Module *, std::string>("import_c", DT_IMPORTC, &import_c_action)},
    {"package", (Directive_Base *) new Directive<IR_Module *, std::string>("package", DT_PACKAGE, &package_action)},
};

// eg. #import "LibC"; -> std/LibC/LibC.kai
void kaic::import_action(IR_Module *m, std::string path)
{
    path = path.substr(1,path.size()-2);
    std::filesystem::path std = KAIC_STD_PATH;
    std::filesystem::path _module = std / path;
    std::filesystem::path unit_path = _module / path;
    std::string unit = unit_path.string() + ".kai";
    
    std::ifstream stream(unit);

    antlr4::ANTLRInputStream input(stream);
    KaiLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    KaiParser parser(&tokens);

    auto tree = parser.file();
    auto ir_module = new IR_Module(unit);
    ir_module->is_root = true;
    Visitor visitor(ir_module);
    visitor.visitFile(tree);

    for (auto decl : ir_module->decls) {
        m->decls.push_back(decl);
    }
    for (auto type : ir_module->types) {
        m->types.push_back(type);
    }
    for (auto inc : ir_module->additional_includes) {
        m->additional_includes.push_back(inc);
    }
    for (auto gt : ir_module->gen_typedefs) {
        m->gen_typedefs.push_back(gt);
    }
}

void kaic::import_c_action(IR_Module *m, std::string path)
{
    path = path.substr(1,path.size()-2);
    m->additional_includes.push_back(KAIC_STD_PATH + "/" + path);
}

void kaic::package_action(IR_Module *m, std::string id)
{
    id = id.substr(1,id.size()-2);
    m->package_id += id + ".";
}

