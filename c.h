#ifndef KAIC_C_H_
#define KAIC_C_H_

// inheriting from this struct means that you have to implement
// the "c" function that returns a string with C code

#include <string>
#include <stdlib.h>

struct C
{
    virtual std::string c(void) = 0;
    virtual std::string c_name(void) { return ""; }
};

#endif // KAIC_C_H_
