#ifndef KAIC__ASSERT_H_
#define KAIC__ASSERT_H_

#include <stdio.h>
#include "log.h"

#define ASSERT(cond, module_, fmt, ...) \
    if (!(cond)) { \
        printf("In file: %s\n", (module_)->source.c_str()); \
        LOG_ERROR_FMT((module_)->current_line, fmt, ##__VA_ARGS__); \
        exit(0); \
    }

#endif // KAIC__ASSERT_H_
