#ifndef KAIC_CONFIG_H_
#define KAIC_CONFIG_H_

#include <filesystem>
#include <string>

#define KAIC_GCC_PATH std::string("gcc")
#define KAIC_STD_PATH kaic_std_path()

#ifndef _KAIC_SOURCE_PATH
#   error "Kai compiler source path is undefined!"
#endif

#define KAIC_SOURCE_PATH std::string(_KAIC_SOURCE_PATH)

static std::string kaic_std_path(void)
{
    std::filesystem::path src = KAIC_SOURCE_PATH;
    std::filesystem::path std = src / "std";
    return std.string();
}

#endif // KAIC_CONFIG_H_
