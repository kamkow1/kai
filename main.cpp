#include <fstream>
#include <string>
#include <numeric>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "antlr4-runtime.h"
#include "backward.hpp"
#include "ir-module.h"
#include "log.h"
#include "KaiLexer.h"
#include "KaiParser.h"
#include "visitor.h"
#include "config.h"

using namespace kaic;

void build_cmd(const char* file_path)
{
    std::ifstream stream(file_path);

    antlr4::ANTLRInputStream input(stream);
    KaiLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    KaiParser parser(&tokens);

    auto tree = parser.file();
    auto ir_module = new IR_Module(file_path);
    ir_module->is_root = true;
    Visitor visitor(ir_module);
    visitor.visitFile(tree);

    std::string fp = file_path;
    std::string c_name = fp.substr(0, fp.find_last_of('.'))+".c";
    std::ofstream out(c_name);
    out << ir_module->c();
    out.close();

    static std::string std_c_modules[] = {
        // KAIC_STD_PATH + "/mibs.c",
        // KAIC_STD_PATH + "/Allocator.c",
    };

    std::vector<std::string> cmd;
    cmd.push_back(KAIC_GCC_PATH);
    cmd.push_back("-I" + KAIC_SOURCE_PATH);
    cmd.push_back("-ggdb");
    cmd.push_back("-w");
    cmd.push_back(c_name);
    for (auto std_c_module : std_c_modules) {
        cmd.push_back(std_c_module);
    }

    std::string full_cmd = std::accumulate(std::next(cmd.begin()), cmd.end(), cmd[0],
        [](std::string a, std::string b) {
            return a + " " + b;
        }
    );
    LOG_INFO_FMT(0, "Running GCC: %s\n", full_cmd.c_str());
    std::system(full_cmd.c_str());
}

int main(int argc, char** argv)
{
    srand(time(0));

    if (argc < 2) {
        LOG_ERROR(0, "No command provided\n");
        return 1;
    }

    std::string cmd = *(++argv);
    
    if (cmd == "compile") {
        if (argc < 3) {
            LOG_ERROR(0, "No file provided\n");
            return 1;
        }
        build_cmd(*(++argv));
    } else {
        LOG_ERROR(0, "Unknown command\n");
    }

    return 0;
}

