#ifndef KAIC_IR_MODULE_H_
#define KAIC_IR_MODULE_H_

#include <vector>
#include "c.h"
#include "gen-typedef.h"

namespace kaic
{

struct Type;
struct Decl;

struct Scope
{
    std::vector<Decl *> decls;
    std::vector<Type *> types;
};

struct IR_Module : C
{
    std::vector<Decl *> decls; // global decls
    std::vector<Type *> types; // global types
    std::vector<Scope *> scopes;
    std::vector<std::vector<std::string>> generics_stack; // stack for generics params
    std::vector<std::string> additional_includes;
    std::vector<Gen_Typedef *> gen_typedefs;
    bool is_root = false; // has main function or not
    std::string package_id;
    std::string source;
    size_t current_line; // for error reporting

    IR_Module(std::string _source):
        source(_source),
        current_line(1)
    {
    }
    std::string c(void) override;
    Decl * get_decl(std::string name); // returns NULL when not found
    Type * get_type(std::string name); // returns NULL when not found
    void make_scope(void);
    void drop_scope(void);
    Scope * get_current_scope(void);
    Decl *get_decl_from_current_scope(std::string name);
    Type *get_type_from_current_scope(std::string name);
    std::string *param_in_generics_stack(std::string name);
    Gen_Typedef * get_gen_typedef(std::string id);
};

}; // namespace kaic

#endif // KAIC_IR_H_
