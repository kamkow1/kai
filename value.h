#ifndef KAIC_VALUE_H_
#define KAIC_VALUE_H_

#include <string>
#include "type.h"
#include "ir-module.h"
#include "decl.h"
#include "_assert.h"
#include "instr.h"
#include "c.h"

namespace kaic
{

struct IR_Module;

enum Value_Type
{
    VT_INT,
    VT_FLOAT,
    VT_REF,
    VT_CALL,
    VT_ADDR,
    VT_DEREF,
    VT_STRING,
    VT_STRUCT,
};

struct Value : Instr
{
    IR_Module *ir_module;
    Type* type;
    Value_Type value_type;

    Value(IR_Module *_ir_module, Value_Type _value_type, Type* _type):
        Instr(IT_VALUE),
        ir_module(_ir_module),
        type(_type),
        value_type(_value_type)
    {
    }
    Value(IR_Module *_ir_module, Value_Type _value_type):
        Instr(IT_VALUE),
        ir_module(_ir_module),
        type(0),
        value_type(_value_type)
    {
    }
};

template<typename T> struct Int_Value : Value
{
    T x;

    Int_Value(IR_Module *_ir_module, T _x):
        Value(_ir_module, VT_INT),
        x(_x)
    {
        std::string name = std::string("Int") + std::to_string(sizeof(T)*8);
        this->Value::type = predefined_types.at(name);
    }
    std::string c(void) override
    {
        return std::to_string(this->x);
    }
};

template<typename T> struct Float_Value : Value
{
    T x;

    Float_Value(IR_Module *_ir_module, T _x):
        Value(_ir_module, VT_FLOAT),
        x(_x)
    {
        std::string name = std::string("Float") + std::to_string(sizeof(T)*8);
        this->Value::type = predefined_types.at(name);
    }
    std::string c(void) override
    {
        return std::to_string(this->x);
    }
};

struct Ref_Value : Value
{
    std::string name;

    Ref_Value(IR_Module *_ir_module, std::string _name):
        Value(_ir_module, VT_REF),
        name(_name)
    {
        auto decl = this->Value::ir_module->get_decl(this->name);
        if (!decl) {
            decl = this->Value::ir_module->get_decl_from_current_scope(name);
        }
        ASSERT(decl, this->Value::ir_module, "Reference to unknown declaration %s\n", this->name.c_str());
        if (decl->decl_type == DT_VAR) {
            this->Value::type = ((Var_Decl *)decl)->var_type;
        } else if (decl->decl_type == DT_FUNC) {
            this->Value::type = ((Func_Decl*)decl)->func_type;
        } else {
            ASSERT(0, this->Value::ir_module, "Reference to unknown declaration type for %s\n", this->name.c_str());
        }
    }
    std::string c(void) override
    {
        return this->name;
    }
};

struct Call_Value : Value
{
    std::string name;
    std::vector<Value *> args;
    std::vector<Type *> generic_args;
    Func_Decl *func_decl;

    Call_Value(IR_Module *_ir_module,
                std::string _name,
                std::vector<Value *> _args,
                std::vector<Type *> _generic_args):
        Value(_ir_module, VT_CALL),
        name(_name),
        args(_args),
        generic_args(_generic_args)
    {
        auto decl = this->Value::ir_module->get_decl(this->name);
        if (!decl) {
            decl = this->Value::ir_module->get_decl_from_current_scope(name);
        }
        ASSERT(decl,this->Value::ir_module,"Call to unknown declaration %s\n",this->name.c_str());
        ASSERT(decl->decl_type==DT_FUNC,this->Value::ir_module,"%s is not a function\n",this->name.c_str());

        auto fdecl = (Func_Decl *)decl;
        this->func_decl = fdecl;
        ASSERT(fdecl->func_type->params.size() == args.size(), this->Value::ir_module,
            "not enough/too much arguments for %s. Expected %zu, got %zu\n",
            this->name.c_str(), fdecl->func_type->params.size(), args.size());

        if (this->generic_args.size() == 0) {
            for (size_t i = 0; i < fdecl->func_type->params.size(); i++) {
                auto type1 = fdecl->func_type->params[i]->param_type;
                auto type2 = args[i]->type;
                ASSERT(check_type(type1,type2),this->Value::ir_module, "Type mismatch of arguments");
            }
        }

        auto t = clone_type(fdecl->func_type->rt_type);

        if (this->generic_args.size() > 0) {
            if (t->type_type == TT_BASIC) {
                auto bt = (Basic_Type *) (t);
                if (bt->is_generic) {
                    size_t i = 0;
                    for (auto ftg : fdecl->func_type->generics) {
                        if (bt->name == ftg) {
                            t = clone_type(this->generic_args[i]);
                        }
                        i++;
                    }
                }
            } else if (t->type_type == TT_STRUCT) {
                auto st = (Struct_Type *) (t);
                st->generic_args = this->generic_args;
            } else {
                ASSERT(0, this->Value::ir_module, "Unhandled case in %s\n", __PRETTY_FUNCTION__);
            }
        }
        this->Value::type = t;
    }
    std::string c(void) override
    {
        std::string s;
        s += this->name;
        s += "(";
        size_t i = 0;
        for (Value *arg : this->args) {
            s += arg->c();
            if (i != this->args.size()-1) {
                s += ", ";
            }
            i++;
        }
        s += ")";
        return s;
    }
};

struct Addr_Value : Value
{
    Value *underying_value;

    Addr_Value(IR_Module *_ir_module, Value *_underlying_value):
        Value(_ir_module, VT_ADDR, new Ptr_Type(_ir_module, _underlying_value->type)),
        underying_value(_underlying_value)
    {
    }
    std::string c(void) override
    {
        std::string s;
        s += "&";
        s += this->underying_value->c();
        return s;
    }
};

struct Deref_Value : Value
{
    Value *underlying_value;

    Deref_Value(IR_Module *_ir_module, Value *_underlying_value):
        Value(_ir_module, VT_DEREF)
    {
        ASSERT(_underlying_value->type->type_type == TT_PTR,this->Value::ir_module,
            "Cannot dereference a non-pointer value\n");
        this->underlying_value = _underlying_value;
        this->Value::type = ((Ptr_Type *) (this->underlying_value->type))->ptr_type;
    }
    std::string c(void) override
    {
        std::string s;
        s += "*";
        s += this->underlying_value->c();
        return s;
    }
};

struct String_Value : Value
{
    std::string _string;

    String_Value(IR_Module *_ir_module, std::string __string):
        Value(_ir_module, VT_STRING, predefined_types.at("String")),
        _string(__string)
    {
    }
    std::string c(void) override
    {
        std::string s;
        s += "make_string (";
        s += this->_string;
        s += ")";
        return s;
    }
};

// TODO: check if field exists in struct
struct Struct_Field_Init : C
{
    std::string name;
    Value *value;

    Struct_Field_Init(std::string _name, Value *_value):
        name(_name),
        value(_value)
    {
    }
    std::string c(void) override
    {
        std::string s;
        s += ".";
        s += this->name;
        s += " = ";
        s += this->value->c();
        return s;
    }
};

struct Struct_Value : Value
{
    Type *struct_type;
    std::vector<Struct_Field_Init> field_inits;

    Struct_Value(IR_Module *_ir_module, Type *_struct_type, std::vector<Struct_Field_Init> _field_inits):
        Value(_ir_module, VT_STRUCT, _struct_type),
        struct_type(_struct_type),
        field_inits(_field_inits)
    {
    }
    std::string c(void) override
    {
        std::string s;
        s += "{";
        size_t i = 0;
        for (auto field_init : this->field_inits) {
            s += field_init.c();
            if (i != this->field_inits.size()-1) {
                s += ", ";
            }
            i++;
        }
        s += "}";
        return s;
    }
};

}; // namespace kaic

#endif // KAIC_VALUE_H_
