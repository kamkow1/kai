#ifndef KAIC_LOG_H_
#define KAIC_LOG_H_

#include <stdio.h>

#define LOG_ERROR(line, msg) printf("Error: (%zu) " msg, (size_t)(line))
#define LOG_ERROR_FMT(line, fmt, ...) printf("Error: (%zu) " fmt, (size_t)(line), ##__VA_ARGS__)
#define LOG_INFO(line, msg) printf("Info: (%zu) " msg, (size_t)(line))
#define LOG_INFO_FMT(line, fmt, ...) printf("Info: (%zu) " fmt, (size_t)(line), __VA_ARGS__)

#endif // KAIC_LOG_H_
