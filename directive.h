#ifndef KAIC_DIRECTIVE_H_
#define KAIC_DIRECTIVE_H_

#include <string>
#include <map>
#include "ir-module.h"

namespace kaic
{

enum Directive_Type
{
    DT_IMPORT,
    DT_IMPORTC,
    DT_PACKAGE,
};

struct Directive_Base
{
    std::string name;
    Directive_Type directive_type;
    
    Directive_Base(std::string _name, Directive_Type _directive_type):
        name(_name),
        directive_type(_directive_type)
    {
    }
};

template <typename... Args> struct Directive : Directive_Base
{
    void (*action)(Args...);

    Directive(std::string _name, Directive_Type _directive_type, void(*_action)(Args...)):
        Directive_Base(_name, _directive_type),
        action(_action)
    {
    }
};

extern std::map<std::string, Directive_Base *> directives;

void import_action(IR_Module *m, std::string path);
void import_c_action(IR_Module *m, std::string path);
void package_action(IR_Module *m, std::string id);

}; // namespace kaic

#endif // KAIC_DIRECTIVE_H_
