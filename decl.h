#ifndef KAIC_GLOBAL_DECL_H_
#define KAIC_GLOBAL_DECL_H_

#include <string>
#include <vector>
#include <algorithm>
#include "type.h"
#include "instr.h"
#include "c.h"
#include "ir-module.h"

namespace kaic
{

enum Decl_Type
{
    DT_FUNC,
    DT_VAR,
    DT_STRUCT,
};

struct Decl : Instr
{
    IR_Module *ir_module;
    std::string name;
    Decl_Type decl_type;

    Decl(IR_Module *_ir_module,
        std::string _name,
        Decl_Type _decl_type,
        Instr_Type _instr_type):
        Instr(_instr_type),
        ir_module(_ir_module),
        name(_name),
        decl_type(_decl_type)
    {
    }
    std::string c_name(void) override
    {
        std::string s = this->name;
        std::replace(s.begin(),s.end(),'.','_');
        return s;
    }
};

struct Func_Decl : Decl
{
    Func_Type *func_type;
    std::vector<Instr *> instrs;
    bool has_defined_body = false; // distinguish between declaration and definition for C

    Func_Decl(IR_Module *_ir_module,
            std::string _name,
            Func_Type *_func_type):
        Decl(_ir_module, _name, DT_FUNC, IT_FUNC_DECL),
        func_type(_func_type)
    {
    }
    std::string c(void) override
    {
        static size_t c_indent = 0;
        std::string s;

        bool is_generic = this->func_type->generics.size() > 0;

        if (is_generic) {
            s += "#define ";
            s += this->Decl::c_name();
            s += "(";
            size_t i = 0;
            for (auto g : this->func_type->generics) {
                s += g;
                if (i != this->func_type->generics.size()-1) {
                    s += ", ";
                }
                i++;
            }
            s += ") ";
        }
        auto rt_type = clone_type(this->func_type->rt_type);
        if (rt_type->type_type == TT_STRUCT) {
            auto st = (Struct_Type *) (rt_type);
            if (st->generics.size() > 0) {
                s += st->c_name();
                s += "(";
                size_t i = 0;
                for (auto g : st->generics) {
                    s += g;
                    if (i != st->generics.size()-1) {
                        s += ", ";
                    }
                    i++;
                }
                s += ") ";
            }
        } else {
            s += rt_type->c();
        }
        s += " ";
        if (this->Decl::name == "main") {
            s += "kai_main";
        } else {
            s += this->Decl::c_name();
        }
        s += " (";
        size_t i = 0;
        for (auto param : this->func_type->params) {
            s += param->param_type->c();
            s += " ";
            s += param->name;
            if (i != this->func_type->params.size()-1) {
                s += ", ";
                i++;
            }
        }
        s += ")";
        if (!this->has_defined_body) {
            s += ";";
        } else {
            if (is_generic) s += " \\";
            s += "\n";
            for (size_t i = 0; i < c_indent; i++) {
                s += "    ";
            }
            s += "{";
            if (is_generic) s += " \\";
            s += "\n";
            c_indent += 1;
            for (auto instr : this->instrs) {
                for (size_t i = 0; i < c_indent; i++) {
                    s += "    ";
                }
                s += instr->c();
                if (instr->instr_type == IT_VALUE) {
                    s += ";";
                }
                if (is_generic) s += " \\";
                s += "\n";
            }
            c_indent -= 1;
            for (size_t i = 0; i < c_indent; i++) {
                s += "    ";
            }
            s += "}";
        }
        return s;
    }
};

struct Value;

struct Var_Decl : Decl
{
    Value* value;
    Type* var_type;

    Var_Decl(IR_Module *_ir_module,
            std::string _name,
            Type* _var_type,
            Value* _value):
        Decl(_ir_module, _name, DT_VAR, IT_VAR_DECL),
        value(_value),
        var_type(_var_type)
    {
    }
    std::string c(void) override;
};



struct Struct_Decl : Decl
{
    Struct_Type *struct_type;

    Struct_Decl(IR_Module *_ir_module,
                std::string _name,
                Struct_Type *_struct_type):
        Decl(_ir_module, _name, DT_STRUCT, IT_STRUCT_DECL),
        struct_type(_struct_type)
    {
    }
    std::string c(void) override
    {
        std::string s;
        s += "#define ";
        s += this->Decl::c_name();
        s += "(";
        size_t i = 0;
        for (auto g : this->struct_type->generics) {
            s += g;
            if (i != this->struct_type->generics.size()-1) {
                s += ", ";
            }
            i++;
        }
        s += ") ";

        if (this->struct_type->generics.size() > 0) {
            s += "struct ";
            s += "{";
            for (auto field : this->struct_type->fields) {
                s += field->c();
                s += "; ";
            }
            s += "}";
        } else {
            s += this->struct_type->c();
        }
        return s;
    }
};

}; // namespace kaic

#endif // KAIC_GLOBAL_DECL_H_
