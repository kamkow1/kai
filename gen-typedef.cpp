#include <string>
#include "gen-typedef.h"
#include "type.h"

using namespace kaic;

std::string Gen_Typedef::c(void)
{
    std::string s;
    s += "typedef ";

    if (this->underlying->type_type == TT_STRUCT) {
        auto st = (Struct_Type *) (this->underlying);
        if (st->generics.size() > 0) {
            s += st->c_name();
            s += "(";
            size_t i = 0;
            for (auto ga : st->generic_args) {
                s += ga->c();
                if (i != st->generic_args.size()-1) {
                    s += ", ";
                }
                i++;
            }
            s += ")";
        } else {
            s += "struct ";
            s += "{";
            for (auto field : st->fields) {
                s += field->c();
                s += "; ";
            }
            s += "}";
        }
    } else {
        s += this->underlying->c();
    }

    s += " ";
    s += this->desired_name;
    s += ";";
    return s;
}

